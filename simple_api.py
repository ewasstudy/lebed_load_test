import uvicorn
from fastapi import FastAPI

iterations = 10000
app = FastAPI()


def calculate_pi(iterations):
    pi = 0
    for i in range(iterations):
        pi += (-1) ** i / (2 * i + 1)
    return 4 * pi



@app.get("/")
def index():
    calculate_pi(iterations)
    return {}


if __name__ == '__main__':
    uvicorn.run(app)
