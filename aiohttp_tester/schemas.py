from hypothesis_jsonschema import from_schema

_EXAMPLE = {
    "type": "object",
    "properties": {
        "name": {
            "type": "string"
        },
        "age": {
            "type": "integer",
            "minimum": 0
        }
    },
    "required": ["name", "age"]
}

# /auth/sign-in
schema_auth_sign_in = from_schema({

})

# /auth/sign-up
schema_auth_sign_up = from_schema({

})

# /users
schema_users = from_schema({

})

# /items
schema_items = from_schema({

})

# /carts
schema_carts = from_schema({

})

# /orders
schema_orders = from_schema({

})
