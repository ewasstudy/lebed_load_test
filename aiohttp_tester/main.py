import asyncio
import random
from multiprocessing import Process

from aiohttp import ClientSession

from schemas import *

COUNT_PROCESSES = 5
TASKS_LIMIT = 100

api_endpoints = {
    "/auth/sign-in": schema_auth_sign_in,
    "/auth/sign-up": schema_auth_sign_up,
    "/users": schema_users,
    "/items": schema_items,
    "/carts": schema_carts,
    "/orders": schema_orders,
}


async def fuck_api():
    async with ClientSession() as session:
        while True:
            if len(asyncio.all_tasks()) > TASKS_LIMIT:
                await asyncio.sleep(1)
                continue

            endpoint, data_gen = random.choice(list(api_endpoints.items()))
            data = data_gen and data_gen.example()

            asyncio.create_task(async_request(session, endpoint, data))


async def async_request(session: ClientSession, ednpoint, data, type="GET"):
    kwargs = {"data" : data } if data else {}
    async with session.request(type, ednpoint, **kwargs) as response:
        print("Status:", response.status)
        print("Content-type:", response.headers['content-type'])

        html = await response.text()
        print("Body:", html[:15], "...")

def main():
    process_pool = []

    for n in range(COUNT_PROCESSES):
        process_pool.append(Process(target=asyncio.run, args=(fuck_api,)))

    for p in process_pool:
        p.run()


if __name__ == '__main__':
    main()
