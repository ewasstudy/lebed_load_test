import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap

from locust_tester.constants import get_filename

# Создание функции для графика
x = np.linspace(0, 10, 100)
y = np.sin(x)


# Определяем градиентную заливку
def gradient_fill(x, y, ax=None, color='blue', alpha_fill=0.3):
    if ax is None:
        ax = plt.gca()

    z = np.linspace(0, 1, 100)
    col = LinearSegmentedColormap.from_list('colormap', [(0, color), (alpha_fill, color), (1, (1, 1, 1, 0))])

    ax.plot(x, y, color=color)
    ax.fill_between(x, y, 0, where=(y >= 0), facecolor=color, alpha=alpha_fill)
    ax.fill_betweenx(y, x, 0, where=(x >= 0), color=color, alpha=0.1)


def plot_with_gradient_fill(x, y):
    # Рисуем линию графика
    plt.plot(x, y, linewidth=2, color='blue')

    # # Создаем градиентную заливку
    # step = 0.05  # Шаг для изменения прозрачности
    # for alpha in np.arange(0, 1 + step, step):
    #     plt.fill_between(x, y, alpha=alpha, color='blue', step='mid', where=[v >= 0 for v in y])


def build_and_save_plot(x: list, y: list, filename):
    # Используем определенную функцию для создания графика
    plot_with_gradient_fill(x, y)

    plt.savefig(filename)
