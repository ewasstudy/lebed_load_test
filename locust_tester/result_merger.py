import json
import os
from collections import defaultdict
from os.path import isfile, join
from typing import TypeVar, TypeAlias, Any

import numpy as np

from locust_tester.constants import RES_DIR, get_filename, get_time_label
from locust_tester.graph_plot import build_and_save_plot

# for path in os.path.


endpointType: TypeAlias = str

statType: TypeAlias = str
valueType: TypeAlias = list
endpointStats: TypeAlias = dict[statType, valueType]

endpoints_stats: dict[endpointType, endpointStats] = defaultdict(lambda: defaultdict(list))

# watchableParams = [
#     "num_failures",
#     "avg_response_time",
#     "current_rps",
# ]

watchableParams = [
    "test",
]

for rel_filename in os.listdir(RES_DIR):
    filename = get_filename(rel_filename)
    if not isfile(filename):
        continue
    with open(filename) as f:
        res = json.load(f)

    for endpoint, v in res.items():
        endpoint_stats: endpointStats = endpoints_stats[endpoint]

        for stat_name in watchableParams:
            endpoint_stats[stat_name].append(v[stat_name])

print(dict(endpoints_stats))

for endpoint, endpoint_stats in endpoints_stats.items():
    time_label = get_time_label()
    endpoint_dir = str(os.path.join(RES_DIR, time_label, endpoint.replace(os.sep, '_')))
    os.makedirs(endpoint_dir)
    was_action = False

    try:

        for stat_name, stat_values in endpoint_stats.items():
            filename = os.path.join(endpoint_dir, f'{stat_name}.png')

            x = list(range(len(stat_values)))
            build_and_save_plot(x, stat_values, filename)
            was_action = True
    finally:
        if not was_action:
            os.rmdir(endpoint_dir)
