import json
from datetime import datetime

import gevent
from locust import HttpUser, events, task
from locust.env import Environment
from locust.log import setup_logging
from locust.stats import stats_history, stats_printer

from locust_tester.constants import get_filename, get_time_label

setup_logging("INFO")


class WebsiteUser(HttpUser):
    host = "http://localhost:8000"

    # wait_time = between(1, 2)

    @task(1)
    def customer(self):
        self.client.get("/customer/")

    @task(1)
    def customer(self):
        self.client.get("/product/")

    @task(1)
    def customer(self):
        self.client.get("/order/")

    @task(1)
    def customer(self):
        self.client.get("/order/details/")


def main():
    # setup Environment and Runner
    env = Environment(user_classes=[WebsiteUser], events=events)
    runner = env.create_local_runner()

    # start a WebUI instance
    web_ui = env.create_web_ui("127.0.0.1", 8089)

    # execute init event handlers (only really needed if you have registered any)
    env.events.init.fire(environment=env, runner=runner, web_ui=web_ui)

    # start a greenlet that periodically outputs the current stats
    gevent.spawn(stats_printer(env.stats))

    # start a greenlet that save current stats to history
    gevent.spawn(stats_history, env.runner)

    # start the test
    runner.start(1, spawn_rate=10)

    # in 30 seconds stop the runner
    gevent.spawn_later(30, runner.quit)

    # wait for the greenlets
    runner.greenlet.join()

    # stop the web server for good measures
    web_ui.stop()

    result = {}
    # Вывод окончательной статистики
    total = env.stats.total.to_dict()
    result[total["name"]] = total

    # Чтобы получить более детальные данные, можно работать с env.stats.entries, который содержит статистику по каждому эндпоинту
    for key, value in env.stats.entries.items():
        entry = value.to_dict()
        result[entry["name"]] = entry

    with open(get_filename(get_time_label(), 'json'), 'w') as f:
        json.dump(result, f)



if __name__ == '__main__':
    main()
