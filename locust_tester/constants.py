import os
from datetime import datetime

RES_DIR = "results"


def get_filename(name: str, ext: str = '') -> str:
    return os.path.join(RES_DIR, name + (f'.{ext}' if ext else ''))


def get_time_label():
    return datetime.now().strftime("%Y%m%d%H%M%S")


if not os.path.exists(RES_DIR):
    os.makedirs(RES_DIR)
